<?php
error_reporting(E_ALL);
include_once("conf.php");
try{
	$pdo_src = new PDO($dsn_src,$src_user_name,$src_password);
        
}catch (Exception $e) {
	var_dump($e);
	echo "数据库链接失败:".$e->getMessage();
}

try{
	$pdo_des = new PDO($dsn_des,$des_user_name,$des_password);
        
}catch (Exception $e) {
	var_dump($e);
	echo "数据库链接失败:".$e->getMessage();
}

function get_data($pdo_src,$email_src_tb,$mark_num = ''){
	$pdo_src = $pdo_src;
	$email_src_tb = $email_src_tb;
	if(preg_match('/puser_us_fb/',$email_src_tb))
	{
		if(isset($mark_num)&&!empty($mark_num))
		{
			$sql_src = "select id,ext_info from $email_src_tb where id > '$mark_num' order by id ;";
		}else{
			$sql_src = "select id,ext_info from $email_src_tb order by id ;";
		}
		$pdo_src->query("set names utf8");
		$rs = $pdo_src->query($sql_src);
		@$rs->setFetchMode(PDO::FETCH_ASSOC);
		$rs_arr = $rs->fetchAll();
		foreach($rs_arr as $k => $v){
			if(!empty($v) && $v != "NULL" ){
				foreach($v as $i => $j){
					$email_src =  json_decode($j,TRUE);
					if(!empty($email_src['email'])) 
					{
						$email_array['email'][] = $email_src['email'];
						$email_array['flag'] = "1";
					}
				}
				$email_array['id'] = $v['id'];
			}else{
				$email_array = '';
			}
		}
	}elseif(preg_match('/user_email_uid_/',$email_src_tb)){
		if(isset($mark_num)&&!empty($mark_num))
                {
			$sql_src = "select id,email from $email_src_tb where id > '$mark_num' order by id;";
		}else{
			$sql_src = "select id,email from $email_src_tb order by id ;";
		}
                $pdo_src->query("set names utf8");
                $rs = $pdo_src->query($sql_src);
                @$rs->setFetchMode(PDO::FETCH_ASSOC);
                $rs_arr = $rs->fetchAll();
		if(count($rs_arr) == 0)
		{
			$email_array = '';
		}else{
			foreach($rs_arr as $k => $v){
				if(is_array($v))
				{
					$email_array['email'][] = $v['email'];
					$email_array['flag'] = "0";
					$email_array['id'] = $v['id'];
				}
			}
		}
	}
	$email_array = empty($email_array) ? '':$email_array;
        return $email_array;
}

function store_data($pdo_des,$email_des_tb,$email,$flag,$send_status){
	$sql_des = "insert into $email_des_tb set email=:email,flag=:flag,send_status=:send_status;";
	$result = $pdo_des->prepare($sql_des);
	if($result) {
		if($result->execute(array(':email'=>$email,':flag'=>$flag,':send_status'=>$send_status)))
		{
			echo "insert successfully";
		}else{
			echo "insert failed";
		}
	}else echo "prepare failed;";
}

function mark_table($table_mark,$email_src_tb,$user_id,$pdo_des){
	$sql = "select table_name from $table_mark where table_name = '$email_src_tb';";
	$pdo_des->query('set names utf8');
	$rs = $pdo_des->query($sql);
	$rs->setFetchMode(PDO::FETCH_ASSOC);
        $rs_arr = $rs->fetchAll();
	foreach($rs_arr as $k => $v)
	{
		$table_name = $v['table_name'];
	}	
	if(!empty($table_name))
	{
		$sql_des = "update $table_mark set mark = :mark where table_name = :table_name;";
	}else{
		$sql_des = "insert into $table_mark(table_name,mark) values(:table_name,:mark);";
	}
        $result = $pdo_des->prepare($sql_des);
	if($result) {
		$result->bindParam(':table_name',$email_src_tb);
		$result->bindParam(':mark',$user_id);
                if($result->execute())
                {
                        echo "@@@@@@@@@@@@@@@ mark insert successfully";
                }else{
			print_r(mysql_error());
                        echo "insert failed";
                }
        }else {
		echo "prepare failed;";
	}
}
	
function get_mark_num($table_name){
	global $pdo_des,$table_mark;
	$sql_des = "select mark from $table_mark where table_name = '$table_name';";
	$pdo_des->query("set names utf8");
	$rs = $pdo_des->query($sql_des);
	$rs->setFetchMode(PDO::FETCH_ASSOC);
	$rs_arr = $rs->fetchAll();
	foreach($rs_arr as $k => $v)
	{
		$mark_num = $v['mark'] == ''?'':$v['mark'];
		return $mark_num;
	}
}


function get_all_data($pdo_src,$pdo_des,$email_des_tb,$table_mark){
	for($i=0;$i<4;$i++){
		$email_src_tb =  "puser_us_fb_".$i;
		$mark_num = get_mark_num($email_src_tb);
		if($email_array = get_data($pdo_src,$email_src_tb,$mark_num))
		{
			foreach($email_array['email'] as $k => $email){
				store_data($pdo_des,$email_des_tb,$email,$email_array['flag'],'0');
			}
			$user_id = $email_array['id'];
			mark_table($table_mark,$email_src_tb,$user_id,$pdo_des);
		}
	}
		
	for($j=0;$j<1;$j++){
                $email_src_tb =  "user_email_uid_".$j;
		$mark_num = get_mark_num($email_src_tb);
		if($email_array = get_data($pdo_src,$email_src_tb,$mark_num))
                {       
                        foreach($email_array['email'] as $k => $email){
                                store_data($pdo_des,$email_des_tb,$email,$email_array['flag'],'0');
                        }
			$user_id = $email_array['id'];
                        mark_table($table_mark,$email_src_tb,$user_id,$pdo_des);
                }
        }
}

get_all_data($pdo_src,$pdo_des,$email_des_tb,$table_mark);
